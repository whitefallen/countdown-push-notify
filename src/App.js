import React from 'react';
import PushNotify from "./PushNotify";

function App() {
  return (
    <div className="App">
      <PushNotify/>
    </div>
  );
}

export default App;
