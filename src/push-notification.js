import firebase from 'firebase';

export const initializeFirebase = () => {
    const firebaseConfig = {
        apiKey: "AIzaSyDqLlHKhpdslkK-_GFMFkykDzNyIUdNYco",
        authDomain: "pokemon-countdown.firebaseapp.com",
        databaseURL: "https://pokemon-countdown.firebaseio.com",
        projectId: "pokemon-countdown",
        storageBucket: "pokemon-countdown.appspot.com",
        messagingSenderId: "312615988827",
        appId: "1:312615988827:web:da844f0e3411fd982f597e"
    };

    firebase.initializeApp(firebaseConfig);

};

export const askForPermissioToReceiveNotifications = async () => {
    try {
        const messaging = firebase.messaging();
        await messaging.requestPermission();
        const token = await messaging.getToken();
        return token;
    } catch (error) {
    }
}
