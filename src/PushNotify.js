import React from 'react';
import { askForPermissioToReceiveNotifications } from './push-notification';

class PushNotify extends React.Component {

    componentDidMount() {
        const token = askForPermissioToReceiveNotifications();
        token.then(function(tokenValue) {
            window.addEventListener('push-notify', function (e) {
                console.log(tokenValue);
                fetch('https://fcm.googleapis.com/fcm/send ', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': process.env.REACT_APP_FIREBASE
                    },
                    body: JSON.stringify({
                        "notification": {
                            "title": "Sword and Shield Countdown",
                            "body": e.detail,
                            "click_action": "%PUBLIC_URL%",
                            "icon": ""
                        },
                        "to": tokenValue
                    })
                });
                console.log("fetch send");
            });
            console.log("listened");
        });
    }
    render() {
        return <div/>;
    }
}

export default PushNotify;
