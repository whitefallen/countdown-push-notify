const daysDiv = document.getElementById('days');
const hoursDiv = document.getElementById('hours');
const minutesDiv = document.getElementById('minutes');
const secondsDiv = document.getElementById('seconds');

let checkDay = Math.floor(initDistant / (day));
let checkHour = Math.floor((initDistant % (day)) / (hour) );
let checkMinute = Math.floor((initDistant % (hour)) / (minute));
let checkSeconds = Math.floor((initDistant % (minute)) / second);

let displayDays = 0;
let displayHours = 0;
let displayMinutes = 0;
let displaySeconds = 0;

if(checkDay > 0) {
        displayDays = checkDay;
}
if(checkHour > 0) {
        displayHours = checkHour;
}
if(checkMinute > 0) {
        displayMinutes = checkMinute;
}
if(checkSeconds > 0) {
        displaySeconds = checkSeconds;
}

daysDiv.innerText = displayDays.toString();
hoursDiv.innerText = displayHours.toString();
minutesDiv.innerText = displayMinutes.toString();
secondsDiv.innerText = displaySeconds.toString();

if(displayDays !== 0 && displayHours !== 0 && displayMinutes !== 0 && displaySeconds !== 0) {
        let countDown = countDownTarget,
            x = setInterval(function() {

                    let now = new Date().getTime(),
                        distance = countDown - now;

                    let days = Math.floor(distance / (day));
                    let hours = Math.floor((distance % (day)) / (hour));

                    let minutes = Math.floor((distance % (hour)) / (minute));
                    let seconds = Math.floor((distance % (minute)) / (second));

                    if(days < 0) {
                            days = 0;
                    }
                    if(hours < 0) {
                            hours = 0;
                    }
                    if(minutes < 0) {
                            minutes = 0;
                    }
                    if(seconds < 0) {
                            seconds = 0;
                    }
                    document.getElementById('days').innerText = days.toString();
                    document.getElementById('hours').innerText = hours.toString();
                    document.getElementById('minutes').innerText = minutes.toString();
                    document.getElementById('seconds').innerText = seconds.toString();

                    let pushText = "";

                    if(days !== 0) {
                            pushText = days.toString() + " Days and " + hours.toString() + " Hours til release of Sword and Shield";
                    } else if(days === 0 && hours !== 0) {
                            pushText = hours.toString() + " Hours til release of Sword and Shield";
                    } else {
                            pushText = "Its release day baby!";
                    }

                    let event = new CustomEvent('push-notify', {
                            detail: pushText
                    });

                    if(days !== 0 && hours === 0 && minutes === 0 && seconds === 0) {
                            window.dispatchEvent(event);
                    } else if(days === 0 && hours !== 0 && minutes === 0 && seconds === 0) {
                            window.dispatchEvent(event);
                    } else if(days !== 0 && hours !== 0 && minutes !== 0 && seconds === 0) {
                            console.log("minutly");
                    }

            }, second);

} else {
        document.getElementById('hide-on-release').style.display = 'none';
        document.getElementById('show-on-release').style.display = 'block';
}


if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('serviceWorker.js');
}

