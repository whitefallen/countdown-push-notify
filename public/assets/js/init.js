const second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24;

const countDownTarget = new Date('Nov 15, 2019 00:00:00').getTime();
const nowTarget = new Date().getTime();
const initDistant = countDownTarget - nowTarget;
