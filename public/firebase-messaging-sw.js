importScripts('https://www.gstatic.com/firebasejs/7.3.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.3.0/firebase-messaging.js');

const firebaseConfig = {
    apiKey: "AIzaSyDqLlHKhpdslkK-_GFMFkykDzNyIUdNYco",
    authDomain: "pokemon-countdown.firebaseapp.com",
    databaseURL: "https://pokemon-countdown.firebaseio.com",
    projectId: "pokemon-countdown",
    storageBucket: "pokemon-countdown.appspot.com",
    messagingSenderId: "312615988827",
    appId: "1:312615988827:web:da844f0e3411fd982f597e"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
